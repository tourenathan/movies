import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:movies/api/movie_api_service.dart';
import 'package:movies/model/Person.dart';

class PersonState with ChangeNotifier {

  Person _person;
  bool _isLoading = true; // determines if the we are loading movies or not
  MovieApiService _movieApiService;
  bool isFailRequest = false;

  PersonState(Person person) {
    this._person = person;
    _movieApiService = MovieApiService();
    getPersonDetails(person.id);
  }

  Person get person => _person;

  set person(Person nperson) {
    _person = nperson;
    notifyListeners();
  }

  bool isLoading() => _isLoading;

  void getPersonDetails(int personID) async{
    ApiResponse response = await _movieApiService.getPersonDetails(personID);
    _isLoading = false;
    if (response.hasResponse) {
      Person newDetails = Person.fromJson(response.results);
      _person.biography = newDetails.biography;
      _person.dateOfBirth = newDetails.dateOfBirth;
      _person.dateOfDeath = newDetails.dateOfDeath;
      _person.placeOfBirth = newDetails.placeOfBirth;
      notifyListeners();
    }
    else {
      isFailRequest = !response.hasResponse;
      //if (movies.length == 0) {}
      notifyListeners();
    }

  }
}