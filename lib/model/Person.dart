import 'package:movies/model/Movie.dart';

class Person {
  final int id;
  final String name;
  final String url;
  final String department;
  final List<Movie> knownFor;
  final int genderId;
  String biography;
  String dateOfBirth;
  String placeOfBirth;
  String dateOfDeath;

  Person({
    this.id,
    this.name,
    this.url,
    this.department,
    this.knownFor,
    this.genderId,
  });

  Person.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        url = (json['profile_path'] != null)
            ? "https://image.tmdb.org/t/p/w500/" + json['profile_path']
            : '',
        department = json['known_for_department'],
        genderId = json['gender'],
        knownFor = (json['known_for']!=null)?(json['known_for'] as List)?.map((item) => Movie.fromJson(item))
            .toList():null,
        biography = json['biography'] ?? null,
        dateOfBirth = json['birthday'] ?? null,
        dateOfDeath = json['deathday'] ?? null,
        placeOfBirth = json['place_of_birth'] ?? null;

  Map<String, dynamic> toJson() => {
        'name': name,
        'url': url,
      };

  getGender() {
    if (genderId == 1) {
      return 'Female';
    } else if (genderId == 2) {
      return 'Male';
    } else {
      return 'Unspecified';
    }
  }
}
