import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:movies/model/Movie.dart';
import 'package:movies/model/Tv.dart';
import 'package:movies/states/movie_state.dart';
import 'package:movies/states/tv_state.dart';
import 'package:provider/provider.dart';

import 'widget/movie_detail_actor_scroller.dart';
import 'widget/movie_detail_header.dart';
import 'widget/movie_detail_photo_scroller.dart';
import 'widget/movie_detail_story.dart';
import 'widget/tv_detail_header.dart';

class DetailPage extends StatelessWidget {
  DetailPage({this.itemId, this.type, this.movieItem, this.tvItem, this.tag});

  final int itemId;
  final int type;
  static bool loaded = false;
  Movie movieItem;
  Tv tvItem;
  String tag;

  static MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    keywords: <String>['movies', 'tv', 'actors', 'cinema'],
    childDirected: false,
    testDevices: <String>[], // Android emulators are considered test devices
  );

  InterstitialAd myInterstitial = InterstitialAd(

    adUnitId: "ca-app-pub-4038752504921943/3210407497",
    targetingInfo: targetingInfo,
    listener: (MobileAdEvent event) {
      print("InterstitialAd event is $event");
      if(event == MobileAdEvent.loaded){
        loaded =  true;
      }
    },
  );

  Future<bool> _goBack(){
    myInterstitial
      ..show(
        anchorType: AnchorType.bottom,
        anchorOffset: 0.0,
        horizontalCenterOffset: 0.0,
      );
 return Future(() => true);
  }
  @override
  Widget build(BuildContext context) {
    myInterstitial
      ..load();
    if (type == 1) {
      return WillPopScope(child: Scaffold(
          body: ChangeNotifierProvider(
        builder: (context) => MovieState(itemId),
        child: Consumer<MovieState>(builder: (context, movieState, _) {

          if(movieState.movie == null && !movieState.isLoading()){
            final snackBar = SnackBar(content: Text('Loading error. Check your internet connection!'));
            Future.delayed(const Duration(milliseconds: 2000), () {
              Scaffold.of(context).showSnackBar(snackBar);
            });

          }else if (movieState.movie != null && !movieState.isLoading()){
            movieItem = movieState.movie;
          }
          return SingleChildScrollView(
              child: Column(
            children: <Widget>[
              MovieDetailHeader(movieItem, tag),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Storyline(movieItem?.storyline),
              ),
              PhotoScroller(movieItem?.photoUrls),
              SizedBox(height: 20.0),
              ActorScroller(movieItem?.actors, 'Actors'),
              SizedBox(height: 20.0),
            ],
          ));
        }),
      )), onWillPop: _goBack);
    } else if (type == 2) {
      return WillPopScope(child: Scaffold(
          body: ChangeNotifierProvider(
              builder: (context) => TvState(itemId),
              child: Consumer<TvState>(builder: (context, tvState, _) {
                if (tvState.tv == null && !tvState.isLoading()) {
                  final snackBar = SnackBar(content: Text('Loading error. Check your internet connection!'));
                  Future.delayed(const Duration(milliseconds: 2000), () {
                    Scaffold.of(context).showSnackBar(snackBar);
                  });
                }else if (tvState.tv != null && !tvState.isLoading()) {
                  tvItem = tvState.tv;
                }

                return SingleChildScrollView(
                    child: Column(
                    children: <Widget>[
                    TvDetailHeader(tvItem, tag),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Storyline(tvItem?.storyline),
                    ),

                    Padding(
                      padding: EdgeInsets.only(left: 18.0, bottom: 18),
                      child: Column(children: [
                        Row(children: [
                          Text('Number of seassons:  ', style: TextStyle(fontSize: 16.0),),
                          Text(tvItem?.seasonCount.toString(),
                            style: Theme.of(context).textTheme.body1.copyWith(
                              //color: Theme.of(context).accentColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),),
                        ],
                      ),
                        SizedBox(height: 10.0),

                        Row(children: [
                          Text('Number of episodes:  ', style: TextStyle(fontSize: 16.0),),
                          Text(tvItem?.episodeCount.toString(),
                            style: Theme.of(context).textTheme.body1.copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),),
                        ],
                        ),
                        (tvItem?.firstAirDate != null)?
                        Padding(child: Row(children: [
                          Text('First air date:  ', style: TextStyle(fontSize: 16.0),),
                          Text(tvItem?.firstAirDate,
                            style: Theme.of(context).textTheme.body1.copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),),
                        ],
                        ),
                        padding: EdgeInsets.only(top: 10.0)):Container(width: 0.0, height: 0.0,),
                      ],),
                    ),
                    PhotoScroller(tvItem?.photoUrls),
                    SizedBox(height: 20.0),
                    ActorScroller(tvItem?.createdBy, "Created by"),
                    SizedBox(height: 20.0),
                    ActorScroller(tvItem?.actors, "Actors"),
                    SizedBox(height: 20.0),

                    ]));
              }))), onWillPop: _goBack);
    } else if (type == 3) {
    } else
      return Container(
        child: Center(
          child: Text("Unknown item"),
        ),
      );
  }
}
