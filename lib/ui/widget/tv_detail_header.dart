import 'package:flutter/material.dart';
import 'package:movies/model/Tv.dart';

import 'movie_detail_arc_banner_image.dart';
import 'movie_detail_header_text.dart';
import 'movie_detail_poster.dart';

class TvDetailHeader extends StatelessWidget{

  TvDetailHeader(this.tv, this.heroTag);

  final Tv tv;
  final String heroTag;

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: <Widget>[
        Padding(
          child: ArcBannerImage(tv.bannerUrl),
          padding: const EdgeInsets.only(bottom: 140.0),
        ),

        Positioned(
          bottom: 0.0,
          left: 16.0,
          right: 16.0,
          top: 0.0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Poster(tv.posterUrl, height: 180.0, tag: heroTag),
              SizedBox(width: 16.0),
              Expanded(child: HeaderText(title: tv.title, rating: tv.rating, starRating: tv.starRating, categories: tv.categories,))
            ],
          ),
        ),
        AppBar(
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(color: Colors.white),
        ),
      ],
    );
  }

}