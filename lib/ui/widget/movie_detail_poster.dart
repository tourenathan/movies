import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class Poster extends StatelessWidget {
  static const POSTER_RATIO = 0.7;

  Poster(
    this.posterUrl, {
      this.tag,
    this.height = 100.0,
  });

  final String posterUrl;
  final double height;
  final String tag;

  @override
  Widget build(BuildContext context) {
    var width = POSTER_RATIO * height;
    return Material(
      elevation: 2.0,
      borderRadius: BorderRadius.circular(4.0),
      child: Hero(tag: tag, child: CachedNetworkImage(
        imageUrl: posterUrl,
        width: width,
        height: height,
        fit: BoxFit.cover,
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),),
    );
  }
}
