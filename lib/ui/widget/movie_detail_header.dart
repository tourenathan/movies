import 'package:flutter/material.dart';
import 'package:movies/model/Movie.dart';

import 'movie_detail_arc_banner_image.dart';
import 'movie_detail_header_text.dart';
import 'movie_detail_poster.dart';

class MovieDetailHeader extends StatelessWidget{

  MovieDetailHeader(this.movie, this.heroTag);
  final Movie movie;
  final String heroTag;

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: <Widget>[
        Padding(
          child: ArcBannerImage(movie?.bannerUrl),
          padding: const EdgeInsets.only(bottom: 140.0),
        ),

        Positioned(
          bottom: 0.0,
          left: 16.0,
          right: 16.0,
          top: 0.0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Poster(movie?.posterUrl, height: 180.0, tag: heroTag),
              SizedBox(width: 16.0),
              Expanded(child: HeaderText(title: movie?.title, rating: movie?.rating, starRating: movie?.starRating, categories: movie?.categories,))
            ],
          ),
        ),
        AppBar(
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(color: Colors.white),
        ),
      ],
    );
  }

}