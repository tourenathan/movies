import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:movies/model/Person.dart';
import 'package:movies/states/person_state.dart';
import 'package:movies/ui/widget/home_page_movie_item.dart';
import 'package:provider/provider.dart';

/// This gives more detail to information on the Person/actor
class DetailPersonProfile extends StatelessWidget {
  DetailPersonProfile({@required this.person, @required this.tag});

  Person person;
  String tag;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: ChangeNotifierProvider(
          builder: (context) => PersonState(person),
          child: Consumer<PersonState>(builder: (context, personState, _) {
            if (personState.isFailRequest && !personState.isLoading()) {
              final snackBar = SnackBar(
                  content:
                      Text('Loading error. Check your internet connection!'));
              Future.delayed(const Duration(milliseconds: 2000), () {
                Scaffold.of(context).showSnackBar(snackBar);
              });
            } else if (!personState.isLoading()) {
              person = personState.person;
            }
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Stack(
                    children: <Widget>[
                      Column(
                        children: [
                          SizedBox(
                            height: 62.0,
                          ),
                          Center(
                              child: Hero(
                                  tag: tag,
                                  child: CircleAvatar(
                                    backgroundImage:
                                        CachedNetworkImageProvider(person.url),
                                    backgroundColor:
                                        Theme.of(context).accentColor,
                                    radius: 100.0,
                                  ))),
                          Center(
                              child: Padding(
                            child: Text(
                              person?.name,
                              style: TextStyle(fontSize: 22.0),
                            ),
                            padding: EdgeInsets.only(top: 12),
                          )),
                          Center(
                              child: Text(
                            person?.department,
                            style:
                                TextStyle(fontSize: 18.0, color: Colors.grey),
                          )),
                        ],
                      ),
                      AppBar(
                        backgroundColor: Colors.transparent,
                        elevation: 0,
                        iconTheme: IconThemeData(
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                      person?.biography != null
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                    padding:
                                        EdgeInsets.only(top: 20.0, bottom: 5.0),
                                    child: Text('Biography',
                                        style: TextStyle(fontSize: 18.0))),
                                Padding(
                                    padding: EdgeInsets.only(bottom: 10),
                                    child: Text(person?.biography,
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.grey)))
                              ],
                            )
                          : Container(),
                      Padding(
                          padding: EdgeInsets.symmetric(vertical: 5.0),
                          child: Row(
                            children: [
                              Text('Gender:  ',
                                  style: TextStyle(fontSize: 17.0)),
                              Text(person?.getGender(),
                                  style: TextStyle(
                                      fontSize: 17.0, color: Colors.grey)),
                            ],
                          )),
                      person?.placeOfBirth != null
                          ? Padding(
                              padding: EdgeInsets.symmetric(vertical: 5.0),
                              child: Wrap(
                                direction: Axis.horizontal,
                                children: [
                                  Text('Place of birth:  ',
                                      style: TextStyle(fontSize: 17.0)),
                                  Text(person?.placeOfBirth,
                                      style: TextStyle(
                                          fontSize: 17.0, color: Colors.grey)),
                                ],
                              ))
                          : Container(),
                      person?.dateOfBirth != null
                          ? Padding(
                              padding: EdgeInsets.symmetric(vertical: 5.0),
                              child: Row(
                                children: [
                                  Text('Date of birth:  ',
                                      style: TextStyle(fontSize: 17.0)),
                                  Text(person?.dateOfBirth,
                                      style: TextStyle(
                                          fontSize: 17.0, color: Colors.grey)),
                                ],
                              ))
                          : Container(),
                      person?.dateOfDeath != null
                          ? Padding(
                              padding: EdgeInsets.symmetric(vertical: 8.0),
                              child: Row(
                                children: [
                                  Text('Date of death:  ',
                                      style: TextStyle(fontSize: 17.0)),
                                  Text(person?.dateOfDeath,
                                      style: TextStyle(
                                          fontSize: 17.0, color: Colors.grey)),
                                ],
                              ))
                          : Container(),
                      Padding(
                          padding: EdgeInsets.symmetric(vertical: 15.0),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Known for:',
                                    style: TextStyle(fontSize: 18.0)),
                                SizedBox.fromSize(
                                  size: const Size.fromHeight(260.0),
                                  child: ListView.builder(
                                    itemCount: person?.knownFor.length,
                                    scrollDirection: Axis.horizontal,
                                    padding: const EdgeInsets.only(
                                        top: 8.0, left: 10.0),
                                    itemBuilder: ((context, i) {
                                      return MovieItem(
                                        tag: person.knownFor[i].id.toString(),
                                        movie: person.knownFor[i],
                                        height: 200.0,
                                      );
                                    }),
                                  ),
                                )
                              ])),
                    ]),
                  ),
                ],
              ),
            );
          }),
        ));
  }
}
